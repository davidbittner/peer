#[macro_use]
extern crate clap;

#[macro_use]
extern crate log;
extern crate simplelog;

extern crate my_internet_ip;

use clap::App;

use simplelog::*;

use std::io;
use std::time::Duration;
use std::fs::File;
use std::io::{
    BufReader,
    prelude::*
};

use std::net::{
    TcpStream,
    TcpListener,
    SocketAddr,
    IpAddr
};

fn upload_file(path: String, port: u32) -> io::Result<()> {
    let path     = std::path::Path::new(&path);
    let file     = File::open(&path)?;
    let mut buff = BufReader::new(file);

    let conn_str = format!("{}:{}", "0.0.0.0", port);
    let conn     = TcpListener::bind(conn_str)?;

    let ip = match my_internet_ip::get() {
        Ok(ip) => match ip {
            IpAddr::V4(addr) => addr,
            IpAddr::V6(_)    => panic!("Received ipv6, shouldn't happen"),
        },
        Err(e) => return Err(io::Error::new(io::ErrorKind::Other, e.to_string())),
    };

    info!("Send this: {}:{}", ip, port);

    let (mut stream, addr) = conn.accept()?;
    info!("New connection from {}:{}.", addr.ip(), addr.port());

    let mut data = Vec::new();
    buff.read_to_end(&mut data)?;

    //yikes
    let file_name = path.file_name().unwrap().to_string_lossy().into_owned();
    info!("Transferring {}", file_name);

    write!(stream, "{}\n", file_name); 
    stream.write_all(&data)?;

    info!("Wrote {} kbs", (data.len() as f32/1000.0));

    Ok(())
}

fn download_file(address: String) -> io::Result<()> {
    let addr: SocketAddr = address.parse().unwrap();

    info!("Attempting connection to {}:{}", addr.ip(), addr.port());
    let stream     = TcpStream::connect_timeout(&addr, Duration::from_secs(5))?;
    let mut reader = BufReader::new(stream);

    let mut file_name = String::new();
    let mut data = Vec::new();

    reader.read_line(&mut file_name)?;
    file_name = String::from(file_name.trim());

    reader.read_to_end(&mut data)?;

    let mut out = File::create(file_name)?;
    out.write_all(&data)?;

    Ok(())
}

fn main() {
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Info, Config::default()).unwrap(),
        ]
    ).unwrap();

    let yaml = load_yaml!("args.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let up_file = matches.value_of("file");
    match up_file {
        Some(val) => {
            let port = matches.value_of("port").unwrap_or("8080");
            let port = port.parse::<u32>()
                .expect(&format!("Given port was not valid: {}", port));

            let upload = upload_file(val.to_string(), port);

            match upload {
                Ok(_)    => info!("Uploaded {} successfully.", val),
                Err(err) => error!("{}", err),
            }
        },
        None => {
            let rec = matches.value_of("receive");
            match rec {
                Some(address) => {
                    let download = download_file(address.to_string());

                    match download {
                        Ok(_)    => info!("File downloaded successfully."),
                        Err(err) => error!("{}", err),
                    }
                },
                None => error!("Invalid parameters given."),
            }
        }
    }
}
